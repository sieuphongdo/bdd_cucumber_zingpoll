$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("RegisterToZingPoll.feature");
formatter.feature({
  "line": 1,
  "name": "Register to ZingPoll",
  "description": "I want to registered successfull",
  "id": "register-to-zingpoll",
  "keyword": "Feature"
});
formatter.scenario({
  "comments": [
    {
      "line": 4,
      "value": "#@register_empty"
    },
    {
      "line": 5,
      "value": "#Scenario: Register with empty data"
    },
    {
      "line": 6,
      "value": "#Given I am on ZingPoll website \"chrome\""
    },
    {
      "line": 7,
      "value": "#And I click the SignIn button"
    },
    {
      "line": 8,
      "value": "#Then The SignIn form should be shown"
    },
    {
      "line": 9,
      "value": "#When I enter email \"\" and password \"\""
    },
    {
      "line": 10,
      "value": "#And I click Login button"
    },
    {
      "line": 11,
      "value": "#Then I verify the failure message \"Please enter your email.\""
    },
    {
      "line": 12,
      "value": "#Then I verify the failure message \"Please enter your password.\""
    }
  ],
  "line": 15,
  "name": "Register with empty new user",
  "description": "",
  "id": "register-to-zingpoll;register-with-empty-new-user",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 14,
      "name": "@Register_with_empty_all_fields"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "I am on ZingPoll website \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "I click the SignIn button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "The SignIn form should be shown",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "Choose task new user radio button",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "The SignUp form should be shown",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "I click Register button",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I verify the failure message \"Please enter your name.\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 26
    }
  ],
  "location": "RegisterPageSteps.i_am_on_ZingPoll_website(String)"
});
formatter.result({
  "duration": 14188082704,
  "status": "passed"
});
formatter.match({
  "location": "RegisterPageSteps.i_click_the_SignIn_button()"
});
formatter.result({
  "duration": 577300379,
  "status": "passed"
});
formatter.match({
  "location": "RegisterPageSteps.the_SignIn_form_should_be_shown()"
});
formatter.result({
  "duration": 642462135,
  "status": "passed"
});
formatter.match({
  "location": "RegisterPageSteps.choose_task_new_user_radio_button()"
});
formatter.result({
  "duration": 499223917,
  "status": "passed"
});
formatter.match({
  "location": "RegisterPageSteps.the_SignUp_form_should_be_shown()"
});
formatter.result({
  "duration": 548735682,
  "status": "passed"
});
formatter.match({
  "location": "RegisterPageSteps.i_click_Register_button()"
});
formatter.result({
  "duration": 762982974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Please enter your name.",
      "offset": 30
    }
  ],
  "location": "RegisterPageSteps.i_verify_the_failure_message(String)"
});
