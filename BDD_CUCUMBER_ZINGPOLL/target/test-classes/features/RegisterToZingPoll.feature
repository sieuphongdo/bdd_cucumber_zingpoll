Feature: Register to ZingPoll
  I want to registered successfull

  #@register_empty
  #Scenario: Register with empty data
    #Given I am on ZingPoll website "chrome"
    #And I click the SignIn button
    #Then The SignIn form should be shown
    #When I enter email "" and password ""
    #And I click Login button
    #Then I verify the failure message "Please enter your email."
    #Then I verify the failure message "Please enter your password."

  @Register_with_empty_all_fields
  Scenario: Register with empty new user
    Given I am on ZingPoll website "chrome"
    And I click the SignIn button
    Then The SignIn form should be shown
    And Choose task new user radio button
    Then The SignUp form should be shown
    When I click Register button
    Then I verify the failure message "Please enter your name."
    Then I verify the failure message "Please enter your email."
    Then I verify the failure message "Please enter your password."
    Then I verify the failure message "The re-type password is required and cannot be empty"
    Then I verify the failure message "Please agree with the term!"
